<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\QuestionController;

Route::get('/', function () {
    return view('index');
});


Route::get('/data-tables', function () {
    return view('data-tables');
});


// pertanyaan

Route::get('/question', 'QuestionsController@index');
Route::get('/question/create', 'QuestionsController@create');
Route::get('/question/{id}', 'QuestionsController@show');
Route::post('/question', 'QuestionsController@store');
Route::get('/question/{id}/edit', 'QuestionsController@edit');
Route::put('/question/{id}', 'QuestionsController@update');
Route::delete('/question/{id}', 'QuestionsController@destroy');

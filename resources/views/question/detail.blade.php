@extends('master')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Detail Pertanyaan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="/question">Kembali</a></li>

        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<div class="card">
  <div class="card-header p-2">

  </div><!-- /.card-header -->
  <div class="card-body">

    <!-- Post -->
    <div class="post">
      <div class="user-block">
        <img class="img-circle img-bordered-sm" src="{{asset('assets/dist/img/user1-128x128.jpg')}}" alt="user image">
        <span class="username">
          <a href="#">Jonathan Burke Jr.</a>

        </span>
        <span class="description">Shared publicly - 7:30 PM today</span>
      </div>
      <!-- /.user-block -->
      <h4>{{ $questions->judul}}</h4>
      <p>
        {{ $questions->isi}}
      </p>

      <p>
        <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
        <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
        <span class="float-right">
          <a href="#" class="link-black text-sm">
            <i class="far fa-comments mr-1"></i> Comments (5)
          </a>
        </span>
      </p>

      <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
    </div>
    <!-- /.post -->
  </div>
</div>
@endsection
@extends('master')

@section('content')
<div class="container-fluid">
  <div class="card card-primary">
    <div class="card-header mt-3 ml-2">
      <h3 class="card-title ">Tambah Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

    <form role="form" method="POST" action="/question">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" placeholder="Silahkan Isi Judul Pertanyaan!" name="judul" value="{{ old ('judul', '') }}">
          @error('judul')
          <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Isi</label>
          <input type="text" class="form-control" rows="3" placeholder="Isikan Pertanyaan Anda!" name="isi" value="{{ old ('isi','') }}"></input>
          @error('isi')
          <div class="alert alert-danger">{{$message}}</div>
          @enderror
        </div>
      </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Create</button>

  </div>
  </form>

</div>
</div>

@endsection
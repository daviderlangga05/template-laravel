@extends('master')

@section('content')
<a href="question/create" class="btn btn-primary btn-lg active mt-3 ml-2" role="button" aria-pressed="true">Tambah Pertanyaan</a>
@if(session('success'))
<div class="alert alert-success">
  {{session('success')}}
</div>
@endif
<table class="table table-responsive-lg table-striped mt-3 ml-2">
  <thead>
    <tr>
      <th scope="col" style="width: 10px;">Nomor</th>
      <th scope="col">Judul</th>
      <th scope="col">isi Pertanyaan</th>
      <th scope="col" style="width: 40px;">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse ( $questions as $key=>$q )
    <tr>
      <th scope="row">{{$key+1}}</th>
      <td>{{ $q->judul}}</td>
      <td>{{$q->isi}}</td>
      <td style="display: flex;">
        <a href="question/{{$q->id}}" class="btn btn-success">Details</a>
        <a href="question/{{$q->id}}/edit" class="btn btn-primary">Edit</a>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger">Delete</button>
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="4" align="center">No Post</td>
    </tr>

    @endforelse
  </tbody>
</table>


<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content bg-danger">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Pertanyaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah Anda Yakin Menghapus Pertanyaan Ini??</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-outline-light">

          <form action="/question/{{$q->id}}" method="post">
            @csrf
            @method('DELETE')
            <input type="submit" value="delete" class="btn btn-danger">
          </form>
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



@endsection
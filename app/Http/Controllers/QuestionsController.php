<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class QuestionsController extends Controller
{
    // list view
    public function index()
    {
        $questions = DB::table('questions')->get();
        return view('question.index', ['questions' => $questions]);
    }


    // create
    public function create()
    {
        return view('question.create');
    }

    // post
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:questions',
            'isi' => 'required',
        ]);
        $query = DB::table('questions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/question')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function show($id)
    {
        $questions = DB::table('questions')->where('id', $id)->first();
        return view('question.detail', compact('questions'));
    }

    // update
    public function edit($id)
    {
        $questions = DB::table('questions')->where('id', $id)->first();
        return view('question.edit', compact('questions'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:questions',
            'isi' => 'required',
        ]);
        $query = DB::table('questions')
            ->where('id', $id)
            ->update([
                'judul' => $request['judul'],
                'isi' => $request['isi']
            ]);
        return redirect('/question')->with('success', 'Pertanyaan Berhasil di Rubah!');
    }

    public function destroy($id)
    {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('question')->with('success', 'Pertanyaan berhasil dihapus');
    }
}
